$(function () {
    $('#email').change(function () {
        $.post('/api/email', { email: $(this).val()}, function (response) {
            console.log(response.success);
            if (!response.success) {
                $('#email').addClass('is-invalid');
            }
        });
    });
});