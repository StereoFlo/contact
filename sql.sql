create table profile
(
  id       int(10) unsigned auto_increment primary key,
  fio      varchar(150) not null,
  sex      tinyint(1)   not null,
  bornDate varchar(20)  not null,
  email    varchar(150) null,
  phone    varchar(20)  null,
  money    int(7)       null,
  about    text         null,
  constraint profile_email_uindex
  unique (email),
  constraint profile_phone_uindex
  unique (phone)
);

