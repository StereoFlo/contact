<?php

/**
 * app routes
 */

use Core\Router\Collection\Route;

return [
    new Route('/^\/$/', \App\Controllers\IndexController::class, 'get', 'form'),
    new Route('/^\/save$/', \App\Controllers\IndexController::class, 'post', 'save'),
    new Route('/^\/api\/email$/', \App\Controllers\ApiController::class, 'post', 'checkEmail'),
];