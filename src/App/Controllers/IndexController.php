<?php

namespace App\Controllers;

use App\Models\ProfileModel;
use Core\Template;
use Core\Request\Request;
use Core\Response\Response;

/**
 * Class IndexController
 * @package Controllers
 */
class IndexController
{
    /**
     * @var Request
     */
    private $req;

    /**
     * IndexController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->req = $request;
    }

    /**
     * @param Template $template
     *
     * @return Response
     * @throws \Exception
     */
    public function form(Template $template): Response
    {
        return Response::create($template->render('form', ['sex' => ProfileModel::SEX_MAP]));
    }

    /**
     * @param ProfileModel $profileModel
     *
     * @return Response
     * @throws \Exception
     */
    public function save(ProfileModel $profileModel)
    {
        $profileModel
            ->setEmail($this->req->request()->get('email'))
            ->setSex($this->req->request()->get('sex'))
            ->setBornDate($this->req->request()->get('bornDate'))
            ->setPhone($this->req->request()->get('phone'))
            ->setFio($this->req->request()->get('fio'))
            ->setMoney($this->req->request()->get('money'))
            ->setMoney($this->req->request()->get('money'))
            ->setPhoto($this->req->files()->get('photo'))
            ->setTxt($this->req->files()->get('txt'))
            ->setAbout($this->req->request()->get('about'))
            ->save();

        return Response::create('OK');
    }
}