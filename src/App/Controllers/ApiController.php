<?php

namespace App\Controllers;

use App\Models\EmailModel;
use Core\Request\Request;
use Core\Response\JsonResponse;

/**
 * Class ApiController
 * @package App\Controllers
 */
class ApiController
{
    /**
     * @var Request
     */
    private $request;

    /**
     * ApiController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param EmailModel $emailModel
     *
     * @return JsonResponse
     */
    public function checkEmail(EmailModel $emailModel)
    {
        $email = $this->request->request()->get('email');
        $res = $emailModel->setEmail($email)->getByEmail();
        return JsonResponse::create(['success' => empty($res)]);
    }
}