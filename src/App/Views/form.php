<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Signin Template for Bootstrap</title>
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/4.0/examples/sign-in/signin.css" rel="stylesheet">
</head>

<body class="text-center">
<form class="form-signin" action="/save" method="post" enctype="multipart/form-data">
    <label for="fio" class="sr-only">Фио</label>
    <input type="text" id="fio" name="fio" class="form-control" placeholder="ФИО" required>
    <label for="sex" class="sr-only">Пол</label>
    <select id="sex" name="sex" class="form-control" required>
        <option>Выберите...</option>
        <?php foreach ($sex as $val => $label) { ?>
                    <option value="<?= $val; ?>"><?= $label;?></option>
        <?php } ?>
    </select>
    <label for="bornDate" class="sr-only">Дата рождения</label>
    <input type="text" id="bornDate" name="bornDate" class="form-control" placeholder="Дата рождения" required>
    <label for="email" class="sr-only">Почта</label>
    <input type="text" id="email" name="email" class="form-control" placeholder="Почта" required>
    <label for="phone" class="sr-only">Номер телефона</label>
    <input type="text" id="phone" name="phone" class="form-control" placeholder="Номер телефона" required>
    <label for="money" class="sr-only">Зарплата</label>
    <input type="text" id="money" name="money" class="form-control" placeholder="Зарплата" required>
    <label for="photo" class="sr-only">Фотография</label>
    <input type="file" id="photo" name="photo" class="form-control" placeholder="Фотография" required>
    <label for="txt" class="sr-only">txt файл</label>
    <input type="file" id="txt" name="txt" class="form-control" placeholder="txt файл" required>
    <label for="about" class="sr-only"> Информация о себе</label>
    <input type="text" id="about" name="about" class="form-control" placeholder=" Информация о себе" required>
    <label for="tags" class="sr-only">Теги</label>
    <input type="text" id="tags" class="form-control" placeholder="Теги" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>