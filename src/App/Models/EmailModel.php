<?php

namespace App\Models;

use Core\Database\DB;

/**
 * Class EmailModel
 * @package App\Models
 */
class EmailModel extends DB
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var \Core\Database\Connection\MysqliConnection|\Core\Database\Connection\PdoConnection
     */
    private $connection;

    /**
     * EmailModel constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->connection = $this->getConnection();
    }

    /**
     * @param mixed $email
     *
     * @return EmailModel
     */
    public function setEmail(string $email): EmailModel
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @todo sanitize email
     * @return array|null
     */
    public function getByEmail(): ?array
    {
        return $this->connection->fetchRow('SELECT email FROM profile where email = \'' . $this->email . '\'');
    }
}