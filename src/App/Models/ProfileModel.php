<?php

namespace App\Models;

use Core\Database\DB;

/**
 * Class ProfileModel
 */
class ProfileModel extends DB
{
    const PATH_TEMPLATE = '%s/uploads/%s';
    const FILE_TEMPLATE = '%s/%s.%s';

    const DOC_TYPE_PHOTO = 'photo';
    const DOC_TYPE_TXT = 'txt';

    const SEX_MEN = 1;
    const SEX_WOMAN = 2;

    const SEX_MAP = [
        self::SEX_MEN   => 'Мужчина',
        self::SEX_WOMAN => 'Женщина',
    ];

    /**
     * @var string
     */
    private $fio;

    /**
     * @var int
     */
    private $sex;

    /**
     * @var string
     */
    private $bornDate;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var int
     */
    private $money;

    /**
     * @var string
     */
    private $about;

    /**
     * @var array
     */
    private $photo = [];

    /**
     * @var array
     */
    private $txt = [];

    /**
     * @var \Core\Database\Connection\MysqliConnection|\Core\Database\Connection\PdoConnection
     */
    private $connection;

    /**
     * ProfileModel constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->connection = $this->getConnection();
    }

    /**
     * @param string $fio
     *
     * @return ProfileModel
     */
    public function setFio(string $fio): ProfileModel
    {
        $this->fio = $fio;
        return $this;
    }

    /**
     * @param int $sex
     *
     * @return ProfileModel
     */
    public function setSex(int $sex): ProfileModel
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @param string $bornDate
     *
     * @return ProfileModel
     */
    public function setBornDate(string $bornDate): ProfileModel
    {
        $this->bornDate = $bornDate;
        return $this;
    }

    /**
     * @param string $email
     *
     * @return ProfileModel
     */
    public function setEmail(string $email): ProfileModel
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param string $phone
     *
     * @return ProfileModel
     */
    public function setPhone(string $phone): ProfileModel
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @param int $money
     *
     * @return ProfileModel
     */
    public function setMoney(int $money): ProfileModel
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @param string $about
     *
     * @return ProfileModel
     */
    public function setAbout(string $about): ProfileModel
    {
        $this->about = $about;
        return $this;
    }

    /**
     * @param mixed $photo
     *
     * @return ProfileModel
     */
    public function setPhoto(?array $photo): ProfileModel
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @param mixed $txt
     *
     * @return ProfileModel
     */
    public function setTxt(?array $txt): ProfileModel
    {
        $this->txt = $txt;
        return $this;
    }

    /**
     * @todo sanitize fields
     * @throws \Exception
     */
    public function save(): self
    {
        $this->uploadFile($this->photo, self::DOC_TYPE_PHOTO);
        $this->uploadFile($this->txt, self::DOC_TYPE_TXT);
        $res = $this->connection->insert("INSERT INTO profile (`fio`, `sex`, `bornDate`, `email`, `phone`, `money`, `about`) VALUES ('".$this->fio."', '".$this->sex."', '".$this->bornDate."', '".$this->email."', '".$this->phone."', '".$this->money."', '".$this->about."');");
        if (!$res) {
            throw new \Exception('something is wromg');
        }
        return $this;
    }

    /**
     * @param array  $data
     * @param string $type
     *
     * @return ProfileModel
     * @throws \Exception
     */
    private function uploadFile(array $data, string $type): self
    {
        $currPath = \sprintf(self::PATH_TEMPLATE, PUBLIC_DIR, $this->email);
        $fileName = $type === self::DOC_TYPE_TXT ? self::DOC_TYPE_TXT : self::DOC_TYPE_PHOTO;
        $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
        if (!\in_array($extension, ['jpg', 'JPG', 'gif', 'GIF', 'png', 'PNG', 'txt', 'TXT',])) {
            throw new \Exception('file that you try to upload is denied by extension');
        }
        if (!\file_exists($currPath)) {
            \mkdir($currPath);
        }
        if (!\move_uploaded_file($data['tmp_name'], \sprintf(self::FILE_TEMPLATE, $currPath, $fileName, $extension))) {
            throw new \Exception('cant move a file');
        }
        return $this;
    }
}